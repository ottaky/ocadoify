const h1 = document.getElementsByTagName("h1")[0],
  interval = setInterval(checkSlots, 1000 * 60 * 5);

checkSlots();

async function checkSlots() {
  const now = new Date();
  const niceTime = `${now.getHours()}:${("0" + now.getMinutes()).slice(-2)}`;

  for (const date of [
    "04-05",
    "04-08",
    "04-11",
    "04-14",
    "04-17",
    "04-20",
    "04-23",
    "04-26",
    "04-29",
    "05-02",
    "05-05",
  ]) {
    h1.innerHTML = `Checking slots for ${date}..`;

    try {
      const response = await fetch(
        `/webshop/getUpdatedSlotBooking.do?ajax=true&date=2020-${date}`,
        {
          credentials: "include",
          headers: {
            Accept: "application/json",
            "X-Requested-With": "XMLHttpRequest",
          },
        }
      );

      const data = await response.json();

      for (const day of data.days) {
        for (const period of ["morning", "afternoon", "evening"]) {
          if (day.slots[period].length) {
            h1.innerHTML = `Found slots at ${niceTime}! ${
              day.date
            } ${period} ${day.slots[period].join(", ")}`;
            document.title = `Found slots at ${niceTime}!`;
            clearInterval(interval);
            return;
          }
        }
      }

      h1.innerHTML = `No slots found at ${niceTime}`;
      document.title = `No slots found at ${niceTime}`;
    } catch (err) {
      h1.innerHTML = `Oops: ${err}`;
    }
  }
}
